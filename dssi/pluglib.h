/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * DSSI bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

NACORE_PRIVATE void
pluglib_load(void *value, void *opaque);

NACORE_PRIVATE void
pluglib_unload(void *value, void *opaque);
