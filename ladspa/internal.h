/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * LADSPA bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include <stdlib.h>
#include <string.h>

#include <math.h>

#ifdef __HAIKU__
#include <FindDirectory.h>
#include <fs_info.h>
#endif

#include <ladspa.h>

#include <NASPRO/brit/lib.h>

#define API	NACORE_PUBLIC NACORE_EXPORT

#include "config.h"

#include "lv2/lv2plug.in/ns/ext/dynmanifest/dynmanifest.h"

#include "bridge.h"
#include "pluglib.h"
#include "plugin.h"
#include "lv2api.h"
