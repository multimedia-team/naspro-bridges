/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * LADSPA bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

static void
port_free(void *value, void *opaque)
{
	nabrit_port port;

	port = (nabrit_port)value;

	free((void *)nabrit_port_get_symbol(port));
}

NACORE_PRIVATE void
plugin_load(nabrit_pluglib pluglib, const LADSPA_Descriptor *desc)
{
	LV2_Descriptor lv2desc;
	nabrit_plugin plugin;
	nabrit_port port;
	char *port_symbol;
	unsigned long i;

	lv2desc = stub_desc;

	nacore_asprintf_nl((char **)&lv2desc.URI, "urn:ladspa:%lu",
			   desc->UniqueID);
	if (lv2desc.URI == NULL)
		return;

	if (desc->activate == NULL)
		lv2desc.activate = NULL;
	if (desc->deactivate == NULL)
		lv2desc.deactivate = NULL;

	plugin = nabrit_plugin_new(bridge, pluglib, &lv2desc);
	if (plugin == NULL)
	  {
		free((void *)lv2desc.URI);
		return;
	  }

	nabrit_plugin_set_opaque(plugin, (void *)desc);

	nabrit_plugin_set_name(plugin, desc->Name);
	nabrit_plugin_set_creator(plugin, desc->Maker);
	nabrit_plugin_set_rights(plugin, desc->Copyright);

	nabrit_plugin_set_is_live(plugin,
		LADSPA_IS_REALTIME(desc->Properties));
	nabrit_plugin_set_in_place_broken(plugin,
		LADSPA_IS_INPLACE_BROKEN(desc->Properties));
	nabrit_plugin_set_hard_rt_capable(plugin,
		LADSPA_IS_HARD_RT_CAPABLE(desc->Properties));

	for (i = 0; i < desc->PortCount; i++)
	  {
		nacore_asprintf_nl(&port_symbol, "port%lu", i);
		if (port_symbol == NULL)
		  {
			nabrit_plugin_free_ports(plugin, port_free, NULL);
			nabrit_plugin_free(bridge, pluglib, plugin);
			return;
		  }

		port = nabrit_port_new(plugin, port_symbol,
			LADSPA_IS_PORT_AUDIO(desc->PortDescriptors[i])
			? nabrit_port_type_audio : nabrit_port_type_control,
			LADSPA_IS_PORT_INPUT(desc->PortDescriptors[i])
			? nabrit_port_direction_in : nabrit_port_direction_out);
		if (port == NULL)
		  {
			free(port_symbol);
			nabrit_plugin_free_ports(plugin, port_free, NULL);
			nabrit_plugin_free(bridge, pluglib, plugin);
			return;
		  }

		nabrit_port_set_name(port, desc->PortNames[i]);

		if (LADSPA_IS_PORT_AUDIO(desc->PortDescriptors[i]))
			continue;

		nabrit_port_set_reports_latency(port,
			!strcmp(desc->PortNames[i], "latency")
			|| !strcmp(desc->PortNames[i], "_latency"));
		nabrit_port_set_toggled(port,
			LADSPA_IS_HINT_TOGGLED(
				desc->PortRangeHints[i].HintDescriptor));
		nabrit_port_set_sample_rate(port,
			LADSPA_IS_HINT_SAMPLE_RATE(
				desc->PortRangeHints[i].HintDescriptor));
		nabrit_port_set_integer(port,
			LADSPA_IS_HINT_INTEGER(
				desc->PortRangeHints[i].HintDescriptor));
		nabrit_port_set_logarithmic(port,
			LADSPA_IS_HINT_LOGARITHMIC(
				desc->PortRangeHints[i].HintDescriptor));

		if (LADSPA_IS_HINT_BOUNDED_BELOW(
				desc->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_min(port,
				desc->PortRangeHints[i].LowerBound);
		if (LADSPA_IS_HINT_BOUNDED_ABOVE(
				desc->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_max(port,
				desc->PortRangeHints[i].UpperBound);

		if (!LADSPA_IS_HINT_HAS_DEFAULT(
				desc->PortRangeHints[i].HintDescriptor))
			continue;

		if (LADSPA_IS_HINT_DEFAULT_MINIMUM(
				desc->PortRangeHints[i].HintDescriptor)
		    && !LADSPA_IS_HINT_SAMPLE_RATE(
				desc->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(port,
				desc->PortRangeHints[i].LowerBound);
		else if (LADSPA_IS_HINT_DEFAULT_LOW(
				desc->PortRangeHints[i].HintDescriptor)
			 && !LADSPA_IS_HINT_SAMPLE_RATE(
				desc->PortRangeHints[i].HintDescriptor))
		  {
			if (LADSPA_IS_HINT_LOGARITHMIC(
					desc->PortRangeHints[i].HintDescriptor))
				nabrit_port_set_deflt(port,
				  exp(0.75
				    * log(desc->PortRangeHints[i].LowerBound)
				    + 0.25
				    * log(desc->PortRangeHints[i].UpperBound)));
			else
				nabrit_port_set_deflt(port,
				  0.75 * desc->PortRangeHints[i].LowerBound
				  + 0.25 * desc->PortRangeHints[i].UpperBound);
		  }
		else if (LADSPA_IS_HINT_DEFAULT_MIDDLE(
				desc->PortRangeHints[i].HintDescriptor)
			 && !LADSPA_IS_HINT_SAMPLE_RATE(
				desc->PortRangeHints[i].HintDescriptor))
		  {
			if (LADSPA_IS_HINT_LOGARITHMIC(
					desc->PortRangeHints[i].HintDescriptor))
				nabrit_port_set_deflt(port,
				  exp(0.5
				    * (log(desc->PortRangeHints[i].LowerBound)
				       + log(desc->PortRangeHints[i].UpperBound)
				      )));
			else
				nabrit_port_set_deflt(port,
				  0.5 * desc->PortRangeHints[i].LowerBound
				  + 0.5 * desc->PortRangeHints[i].UpperBound);
		  }
		else if (LADSPA_IS_HINT_DEFAULT_HIGH(
				desc->PortRangeHints[i].HintDescriptor)
			 && !LADSPA_IS_HINT_SAMPLE_RATE(
				desc->PortRangeHints[i].HintDescriptor))
		  {
			if (LADSPA_IS_HINT_LOGARITHMIC(
					desc->PortRangeHints[i].HintDescriptor))
				nabrit_port_set_deflt(port,
				  exp(0.25
				    * log(desc->PortRangeHints[i].LowerBound)
				    + 0.75
				    * log(desc->PortRangeHints[i].UpperBound)));
			else
				nabrit_port_set_deflt(port,
				  0.25 * desc->PortRangeHints[i].LowerBound
				  + 0.75 * desc->PortRangeHints[i].UpperBound);
		  }
		else if (LADSPA_IS_HINT_DEFAULT_MAXIMUM(
				desc->PortRangeHints[i].HintDescriptor)
			 && !LADSPA_IS_HINT_SAMPLE_RATE(
				desc->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(port,
				desc->PortRangeHints[i].UpperBound);
		else if (LADSPA_IS_HINT_DEFAULT_0(
				desc->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(port, 0.0);
		else if (LADSPA_IS_HINT_DEFAULT_1(
				desc->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(port, 1.0);
		else if (LADSPA_IS_HINT_DEFAULT_100(
				desc->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(port, 100.0);
		else if (LADSPA_IS_HINT_DEFAULT_440(
				desc->PortRangeHints[i].HintDescriptor))
			nabrit_port_set_deflt(port, 440.0);
	  }
}

NACORE_PRIVATE void
plugin_unload(void *value, void *opaque)
{
	nabrit_plugin plugin;
	LV2_Descriptor *desc;

	plugin = (nabrit_plugin)value;

	nabrit_plugin_free_ports(plugin, port_free, NULL);

	desc = nabrit_plugin_get_descriptor(plugin);

	free((void *)desc->URI);
}
