/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A collection of bridges to LV2
 * LADSPA bridge
 *
 * Copyright (C) 2007-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include "internal.h"

typedef struct
  {
	LADSPA_Handle		 handle;
	LADSPA_Descriptor	*descriptor;
  } instance_t;

static LV2_Handle
instantiate(const LV2_Descriptor *descriptor, double sample_rate,
	    const char *bundle_path, const LV2_Feature * const *features)
{
	instance_t *ret;
	LADSPA_Descriptor *ldesc;

	ldesc = nabrit_plugin_get_opaque(
			nabrit_plugin_from_descriptor(descriptor));

	ret = malloc(sizeof(instance_t));
	if (ret == NULL)
		return NULL;

	ret->descriptor = ldesc;
	ret->handle = ldesc->instantiate(ldesc, sample_rate);
	if (ret->handle == NULL)
	  {
		free(ret);
		return NULL;
	  }

	return (LV2_Handle)ret;
}

static void
connect_port(LV2_Handle instance, uint32_t port, void *data_location)
{
	instance_t *i;

	/* This is needed to avoid a little incompatibility beetween LADSPA and
	   LV2. LADSPA's connect_port() does not specify whether data_location
	   is valid at the time it is being run, while LV2 mandates the plugin
	   not to trust the memory location indicated by the pointer at the time
	   connect_port() is run. */
	if (data_location == NULL)
		return;

	i = (instance_t *)instance;

	i->descriptor->connect_port(i->handle, port, data_location);
}

static void
activate(LV2_Handle instance)
{
	instance_t *i;

	i = (instance_t *)instance;

	i->descriptor->activate(i->handle);
}

static void
run(LV2_Handle instance, uint32_t sample_count)
{
	instance_t *i;

	i = (instance_t *)instance;

	i->descriptor->run(i->handle, sample_count);
}

static void
deactivate(LV2_Handle instance)
{
	instance_t *i;

	i = (instance_t *)instance;

	i->descriptor->deactivate(i->handle);
}

static void
cleanup(LV2_Handle instance)
{
	instance_t *i;

	i = (instance_t *)instance;

	i->descriptor->cleanup(i->handle);

	free(i);
}

NACORE_PRIVATE LV2_Descriptor stub_desc =
  {
	/* .URI			= */ NULL,
	/* .instantiate		= */ instantiate,
	/* .connect_port	= */ connect_port,
	/* .activate		= */ activate,
	/* .run			= */ run,
	/* .deactivate		= */ deactivate,
	/* .cleanup		= */ cleanup,
	/* .extension_data	= */ NULL
  };

API const LV2_Descriptor *
lv2_descriptor(uint32_t index)
{
	return nabrit_bridge_get_descriptor(bridge, index);
}
